//
//  GameScene.swift
//  Project14
//
//  Created by Hudzilla on 15/09/2015.
//  Copyright (c) 2015 Paul Hudson. All rights reserved.
//

import GameplayKit
import SpriteKit

class GameScene: SKScene {
    
    var vc : GameViewController?
    
    // MARK: Properties
    
    var slots = [WhackSlot]()
    var popupTime = 0.85
    var gamebackgroundmusic : SKAudioNode!
    
    var gameLives: SKLabelNode!
    var lives: Int = 3 {
        didSet {
            gameLives.text = "Lives: \(lives)"
        }
    }
    
    var numRounds = 0
    
    var gameScore: SKLabelNode!
    var score: Int = 0 {
        didSet {
            gameScore.text = "Score: \(score)"
        }
    }
    
    
    // MARK: Methods
    override func didMoveToView(view: SKView) {
        
        
        gamebackgroundmusic = SKAudioNode(fileNamed: "background")
        //addChild(gamebackgroundmusic)
        
        let background = SKSpriteNode(imageNamed: "whackBackground")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .Replace
        background.zPosition = -1
        addChild(background)
        
        gameScore = SKLabelNode(fontNamed: "Chalkduster")
        gameScore.text = "Score: 0"
        gameScore.position = CGPoint(x: 8, y: 8)
        gameScore.horizontalAlignmentMode = .Left
        gameScore.fontSize = 48
        addChild(gameScore)
        
        gameLives = SKLabelNode(fontNamed: "Chalkduster")
        gameLives.text = "lives: 3"
        gameLives.position = CGPoint(x: 800, y: 8)
        gameLives.horizontalAlignmentMode = .Left
        gameLives.fontSize = 48
        addChild(gameLives)
        
        for i in 0 ..< 5 { createSlotAt(CGPoint(x: 100 + (i * 170), y: 410)) }
        for i in 0 ..< 4 { createSlotAt(CGPoint(x: 180 + (i * 170), y: 320)) }
        for i in 0 ..< 5 { createSlotAt(CGPoint(x: 100 + (i * 170), y: 230)) }
        for i in 0 ..< 4 { createSlotAt(CGPoint(x: 180 + (i * 170), y: 140)) }
        
        
        NSTimer.scheduledTimerWithTimeInterval(1 , target: self, selector: "createEnemy", userInfo: nil, repeats: false)
    
        
        
    }
    
    
    func createSlotAt(pos: CGPoint) {
        let slot = WhackSlot()
        slot.configureAtPosition(pos)
        addChild(slot)
        slots.append(slot)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
        // gamebackgroundmusic.runAction(SKAction.stop())
        
        
        if let touch = touches.first {
            let location = touch.locationInNode(self)
            let nodes = nodesAtPoint(location)
            
            for node in nodes {
                if node.name == "charFriend" {
                    let whackSlot = node.parent!.parent as! WhackSlot
                    if !whackSlot.visible { continue }
                    if whackSlot.isHit { continue }
                    
                    whackSlot.hit()
                    lives -= 1
                    
                    
                    runAction(SKAction.playSoundFileNamed("whackBad.caf", waitForCompletion:false))
                } else if node.name == "charEnemy" {
                    let whackSlot = node.parent!.parent as! WhackSlot
                    if !whackSlot.visible { continue }
                    if whackSlot.isHit { continue }
                    
                    whackSlot.charNode.xScale = 0.85
                    whackSlot.charNode.yScale = 0.85
                    
                    whackSlot.hit()
                    score += 1
                    
                    runAction(SKAction.playSoundFileNamed("gunshot", waitForCompletion:false))
                    
                    
                }
            }
            
        }
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    
//    func createEnemy2() {
//        numRounds += 1
//        
//        if score < 15 {
//            
//            
//            let winScene = SKScene(fileNamed: "WinScene.sks")
//            view?.presentScene(winScene)
//            dispatch_get_main_queue()
//            
//            
//            
//            return
//            
//        }
//    }
    
    func createEnemy() {
        numRounds += 1
        
        if lives == 0 {
  
            let loseScene = LoseScene(fileNamed: "LoseScene.sks")
            loseScene?.vc = vc
            
            view?.presentScene(loseScene)

            dispatch_get_main_queue()

            return
            
        }
        
        if score >= 15 {
            let winScene = WinScene(fileNamed: "WinScene.sks")
            
            winScene?.vc = vc
            
            view?.presentScene(winScene)
            dispatch_get_main_queue()
            
            return
        }

        if numRounds >= 30 {
            for slot in slots {
                slot.hide()
            }
            
            let gameOver = SKSpriteNode(imageNamed: "gameOver")
            gameOver.position = CGPoint(x: 512, y: 384)
            gameOver.zPosition = 1
            
            
            addChild(gameOver)
            
            return
        }

        popupTime *= 0.991
        
        slots = GKRandomSource.sharedRandom().arrayByShufflingObjectsInArray(slots) as! [WhackSlot]
        slots[0].show(hideTime: popupTime)
        
        if RandomInt(min: 0, max: 12) > 4 { slots[1].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 8 {    slots[2].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 10 { slots[3].show(hideTime: popupTime) }
        if RandomInt(min: 0, max: 12) > 11 { slots[4].show(hideTime: popupTime)    }
        
        let minDelay = popupTime / 2.0
        let maxDelay = popupTime * 2
        
        let delay = RandomDouble(min: minDelay, max: maxDelay)
     
        RunAfterDelay(delay) { [unowned self] in
            self.createEnemy()

        }
        
        
        /*
        NSTimer *t = [NSTimer scheduledTimerWithTimeInterval: 2.0
        target: self
        selector:@selector(onTick:)
        userInfo: nil repeats:NO];
        */
        
    }
}



























